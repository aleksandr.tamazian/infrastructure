# Infrastructure

### Docker: image vs container
`Docker image` - application we want to run

`Docker container` - running instance of this image as a isolated process

#### Docker image
>Docker images themselves are never “started” and never “running”. The docker run command takes the Docker image as a template and produces a container from it.

* Images are created from a Dockerfile with the docker build command.
* Images are stored in a Docker registry, such as Docker Hub and can be downloaded with the docker pull command:
```
$ docker pull ubuntu
Using default tag: latest
latest: Pulling from library/ubuntu
50aff78429b1: Pull complete 
f6d82e297bce: Pull complete 
275abb2c8a6f: Pull complete 
9f15a39356d6: Pull complete 
fc0342a94c89: Pull complete 
Digest: sha256:ec0e4e8bf2c1178e025099eed57c566959bb408c6b478c284c1683bc4298b683
Status: Downloaded newer image for ubuntu:latest
```

To see downloaded instances, use comand:
```
docker images
```

#### Image layers
Docker images are designed to be composed of a series of layers.
Each instruction in a __Dockerfile__ creates a layer in the image.
Each layer is a set of differences from the previous layer.

To check layers, use:
```
docker history $IMAGE_ID$
```

#### Docker container
Containers are created from images with the `docker run` command and can be listed with the `docker ps` command.

To create a container, Docker engine takes an image, adds the top writable layer and initializes various settings (network ports container name, ID and resource limits).

All write operation inside the container are stored in this writable layer, so when the container is deleted, the writable layer is also deleted while the underlying image remains unchanged.

### Container - just a process
Docker container - lightweight and isolated process.

Let's start container with `mongo db`.
```
docker run --name mongo -d mongo
```
After that, we can see this process via docker CLI:
```
docker ps -a
```
Or
```
docker container ps
```

If container with mongo is a process, how we can find it?
If you are running Linux machine, there is no problems, just check it via `ps`:
```
ps aux | grep mongo
```

And we will get something like:

<a href="https://ibb.co/s6xxk9k"><img src="https://i.ibb.co/fn66BdB/2020-05-27-19-26-49.png" alt="2020-05-27-19-26-49" border="0"></a>

Or `top | grep mongo`:

<a href="https://ibb.co/dkqCJbq"><img src="https://i.ibb.co/7r5MNg5/2020-05-27-19-30-13.png" alt="2020-05-27-19-30-13" border="0"></a>

Now we can see, that docker containers just a processes and no more (for our operation system).

Another intresting point about docker on macos X. 
If i will start docker container on mac machine and then use `ps aux | grep $CONTAINER_NAME$`, then i will se no process. That's happend because Docker for mac [use mini virtual machine with linux](https://www.bretfisher.com/docker-for-mac-commands-for-getting-into-local-docker-vm/).

### Docker layers
When docker deamon generates container, it use special image layers.

Image layer - immutable data, which also called "intermidiate" image. 

Docker images comprise of readable layers stacked on top of each other. These layers are shared across different containers. Each of these layers are also termed as the “image”. Thus, a docker image of any software package can comprise of multiple images (layers) such as bootfs, rootfs etc., stacked on top of each other. Following are some of the key images which form part of any docker image:
1. bootfs
2. rootfs (base image resembling linux/unix/windows filesystem)
3. Other images which are added on top of the rootfs (base image)

Following image shows up the layers in [Union FileSystem](https://en.wikipedia.org/wiki/UnionFS) (Docker image filesystem) which are stacked on top of each other in a Docker image. Base image represents the rootfs. All the layers except container are read-only.

<a href="https://imgbb.com/"><img src="https://i.ibb.co/dDNLDNm/image.png" alt="image" border="0"></a>

Docker containers represent a read-write filesystem which is mounted by docker on top of images’ layers. This is the layer where docker container processes will read/write the data. Any files of underlying layers when changed will be copied into the read-write layer of the container. Any new files will also be read into the read-write layer. When the container is removed, this read-write filesystem layer is removed.  It implies that multiple containers can share the same with unique writable layers as shown in figure below. Following diagram represents the docker image having images of Apache and emacs stacked over each other, and, a writable layer for container.

<a href="https://imgbb.com/"><img src="https://i.ibb.co/NYk9RG9/image.png" alt="image" border="0"></a>

For example. let's create next [dockerfile](https://docs.docker.com/engine/reference/builder/)
```
FROM ubuntu
RUN touch first.txt
```

Then let's build container with image from dockerfile:
```
docker build . 
```

Then let's check image layers with next command via container id (`da852782df14`):
```
$ docker history da852782df14
IMAGE               CREATED             CREATED BY                                      SIZE                COMMENT
da852782df14        2 minutes ago       /bin/sh -c touch first.txt                      0B                  
1d622ef86b13        5 weeks ago         /bin/sh -c #(nop)  CMD ["/bin/bash"]            0B                  
<missing>           5 weeks ago         /bin/sh -c mkdir -p /run/systemd && echo 'do…   7B                  
<missing>           5 weeks ago         /bin/sh -c set -xe   && echo '#!/bin/sh' > /…   811B                
<missing>           5 weeks ago         /bin/sh -c [ -z "$(apt-get indextargets)" ]     1.01MB              
<missing>           5 weeks ago         /bin/sh -c #(nop) ADD file:a58c8b447951f9e30…   72.8MB   
```

Then let's change dockerfile and build it:
```
FROM ubuntu
RUN touch second.txt
```

And check history:
```
$ docker history ce5df0c8135e
IMAGE               CREATED             CREATED BY                                      SIZE                COMMENT
ce5df0c8135e        22 seconds ago      /bin/sh -c touch second.txt                     0B                  
1d622ef86b13        5 weeks ago         /bin/sh -c #(nop)  CMD ["/bin/bash"]            0B                  
<missing>           5 weeks ago         /bin/sh -c mkdir -p /run/systemd && echo 'do…   7B                  
<missing>           5 weeks ago         /bin/sh -c set -xe   && echo '#!/bin/sh' > /…   811B                
<missing>           5 weeks ago         /bin/sh -c [ -z "$(apt-get indextargets)" ]     1.01MB              
<missing>           5 weeks ago         /bin/sh -c #(nop) ADD file:a58c8b447951f9e30…   72.8MB          
```

We can see, that layer with ubuntu creation is the same (`1d622ef86b13`). Because image layers immutable, then docker deamon can cache some layers and then reuse.

### Linux namespaces and isolation
[Docker makes](https://medium.com/@BeNitinAgarwal/understanding-the-docker-internals-7ccb052ce9fe#:~:text=Namespaces,of%20namespaces%20for%20that%20container.&text=Each%20aspect%20of%20a%20container,is%20limited%20to%20that%20namespace.) use of kernel namespaces to provide the isolated workspace called the container. When you run a container, Docker creates a set of namespaces for that container. These namespaces provide a layer of isolation. Each aspect of a container runs in a separate namespace and its access is limited to that namespace.

Let's compare VM's and Containers once again:

<a href="https://ibb.co/wyNngq2"><img src="https://i.ibb.co/cJcW39s/image.png" alt="image" border="0"></a>

So, containers [consists of](https://stackoverflow.com/questions/34820558/difference-between-cgroups-and-namespaces) `cgroups` and `namespaces`. 
>__Cgroups__ = limits how much you can use. __Namespaces__ = limits what you can see (and therefore use)

Cgroups (or control groups) involve resource metering and limiting:
1. memory
2. CPU
3. block I/O
4. network

<a href="https://ibb.co/1KpfQzM"><img src="https://i.ibb.co/S01Xyfc/image.png" alt="image" border="0"></a>

Namespaces provide processes with their own view of the system

Multiple namespaces:
1. pid
2. net
3. mnt
4. uts
5. ipc
6. user

__Each process is in one namespace of each type__.

<a href="https://ibb.co/HH48c2n"><img src="https://i.ibb.co/nLmJX0z/image.png" alt="image" border="0"></a>

Example:

<a href="https://ibb.co/ZVs608h"><img src="https://i.ibb.co/wRHNfWK/image.png" alt="image" border="0"></a>

In this example, `hadop` process with id (pid) `109` cannot access `bitcoin` process with id `52`, because it's different namespaces isolation. Also, all this PIDs limited and controlled by `cpu` and `memory` cgroups. And important addition: with PID namespace isolation, processes in the child namespace have no way of knowing of the parent process’s existence. However, processes in the parent namespace have a complete view of processes in the child namespace, as if they were any other process in the parent namespace.

<a href="https://ibb.co/thRWHb4"><img src="https://i.ibb.co/rt91QZm/image.png" alt="image" border="0"></a>


